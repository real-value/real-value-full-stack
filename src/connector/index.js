const connector = require("./connector")
const producer = require("./producer")

module.exports = {
    connector,
    producer
}