
export const NumericStyle = {
    width: '25%',
    padding: '15px',
    background: "#F6FAF4",
    borderStyle: 'none',
    height: '40px'
  }

export const LabelStyle = {
    display: 'inline-block',
    fontWeight: 'bold',
    paddingBottom: '10px'
  }
  
export const TextStyle = {
    width: '100%',
    background: "#F6FAF4",
    borderStyle: 'none',
    height: '50px',
    padding: '15px'
  }


export const consoleStyle = {
    border: "1px solid #84bd00",
    fontFamily: "monospace",
    fontSize: "13px",
    lineHeight: "1.4",
    width: "100%",
    minHeight: "150px",
    padding: "10px"
  };

  

